## Scene 03: post.sif

## Voiceover: 

"Any person may ask for information without having to give a particular reason - the authorites have to answer them."

## Scene Description:

The screen is divided into quadrants.  
1. At top left, a citizen posts their FOI request into a postbox.  
2. At bottom left, the letter falls into the in-tray of the official they wrote to. The official picks up and acknowledges the letter.  
3. At bottom right, the official places their response to the FOI request in their out-tray. Another official/coworker collects the letter.  
4. At top right, a postal worker posts the official's response into the citizen's letterbox.  


## NOTES:  
The letters in this sequence move between different layers and groups (eg under thumbs but above fingers, or over the postbox but under the slot cover).  
This means that each letter is duplicated a couple of times and the opacity ('Amount') of each letter copy is set to either 0 or 1 depending if that particular copy should be visible or not. These letter copies are listed in the order which they appear:  
- citizen letter 01 (in the citizen's hand, posted in the postbox)  
- citizen letter 02 (falling into the official's in-tray)  
- citizen letter 03 (in the official's left hand)  
- official letter 04 (in the official's right hand)  
- official letter 05 (in the official's out-tray)  
- official letter 06 (in the postie's hand, posted in the letterbox)  

Naming of layers in the project should hopefully be self-explanatory (with a little exploration).    
The following abbreviations refer to the screen quadrant in which the action takes place:    
TL - Top Right  
BL - Bottom Right  
BL - Bottom Left  
BR - Bottom Right  


## Changes for international contexts:  

POSTBOX     
change colours to match local postal service:   

		/postbox (TL)/
			lip
			card
			slotcover/slotcover
			front plane
			top plane
			r side plane

change or replace the postal horn if necessary:
	
		/postbox (TL)/
			wide rim
			horn
			mouthpiece
			neck

OFFICE  
replace/remove German text on the trays:

		/official desk (BL + BR)/trays front/
			Text - eingang (BR)/
			Text - ausgang (BR)/

replace/remove the German 'Bundesadler' (Federal Eagle, the sign of the German government)

		/official desk (BL + BR)/in-tray + desk/document/
			german federal eagle

POSTIE  
change sleeve colour to match local postal service: 

		/postie + official letter 06 (TR)/postie sleeve/
			texture/sleeve
			cuff

ENVELOPES  	
The addresses on the envelopes are very unlikely to be read by the audience, but they are legible if you pause the video and squint :)  
The official letter also has another 'Bundesadler' on it. Perfectionists may want to change this:

    	/citizen letter 02 (BL)/citizen letter 02 (in tray)/citizen letter
	    		address/
	    /citizen hand + letter 01 (TL)/citizen hand + letter 01/citizen letter 01/letter 01/
	    		address/
	    /official + citizen letter 03 (BL)/citizen letter 03 (in hand)/letter 03/
	    		address/
	    /official desk (BL + BR)/official + official letter 04 (BR)/official letter 04/
	    		address/
	    		german federal eagle
	    /official desk (BL + BR)/official letter 05 (BR)/
	    		address/
	    		german federal eagle
	    /postie + official letter 06 (TR)/official letter 06/letter 06/
		    	address/
		    	german federal eagle