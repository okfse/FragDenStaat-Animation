[/svg](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/svg)
[Inkscape file format](http://wiki.inkscape.org/wiki/index.php/Frequently_asked_questions#Are_Inkscape.27s_SVG_documents_valid_SVG.3F)  
The illustrations for this video were created in [Inkscape](http://inkscape.org)  - mostly by me, but with many contributions from [Judith Carnaby](http://judithcarnaby.com).
Personally, I find Inkscape more flexible and intuitive for creating vector images than Synfig, so I create my images in Inkscape and then save a copy as a .sif:  
(File>Save As... Synfig Animation *.sif)  
In order for a complex object to be grouped and labeled when you open it in Synfig, it's a good idea to put each object on its own (labeled) layer. Otherwise when you open the .sif you just see hundreds of anonymous layers named _path69302_, _path69303_, and so on. You can also group and rename objects within Synfig, it just may be confusing to find what you're looking for.
