[/fonts](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/fonts)  
A large part of adapting the project for international use is replacing German-language text with the local language. For this, you can edit and replace the text in each scene's Inkscape .svg project.  
The FdS website uses [Kreon](https://www.google.com/fonts/specimen/Kreon) as its main font, hence its use here, though you may want to choose another font to fit your site's branding and design.  
[Pecita](http://pecita.eu/) and [NotCourierSans](http://ospublish.constantvzw.org/foundry/notcouriersans/) have been used for the addresses on the envelopes in scene '[03 Post](https://gitlab.com/cameralibre/FragDenStaat-Animation/blob/master/svg/)'.  
[eufm10](http://www.ams.org/publications/authors/tex/amsfonts), [League Gothic](https://www.theleagueofmoveabletype.com/league-gothic), and [Cardo](http://www.scholarsfonts.net/cardofnt.html) are used for the newspaper in scene '[09 Combining Data](https://gitlab.com/cameralibre/FragDenStaat-Animation/blob/master/svg/)'.


When converting an Inkscape .svg to a Synfig .sif, text is converted to vector paths, so you don't require these fonts if you're just working on the Synfig files, as they contain no text information.
