## About

'Frag den Staat' is a vector animation created using the Free Software tools [Synfig Studio](synfig.org) and [Inkscape](inkscape.org).  
You can see the original video in its entirety at [Camera Libre](http://cameralibre.cc/fragdenstaat)  
I made the original in 2014 - now I'm releasing and documenting the project files to allow other people in other countries to use and adapt the video.

It's designed to explain and promote the German Freedom of Information website [Frag den Staat](http://fragdenstaat.de) ("Ask the State") as well as the idea of [Freedom of Information](https://en.wikipedia.org/wiki/Freedom_of_information) more generally.  
Frag den Staat was set up by [Stefan Wehrmeyer](http://stefanwehrmeyer.com/) and [Open Knowledge Foundation Deutschland](http://okfn.de).  
Stefan also created [FROIDE](https://github.com/stefanw/froide), the software which FdS is built on.  
FROIDE and the similar software project [Alaveteli](http://alaveteli.org/) are the basis of a number of FOI websites around the world, who share a common goal, and a common approach: to strengthen transparency and democracy, by enabling citizens to exercise their right to information about public bodies.
Hopefully this animation can be adapted and improved to serve their purposes.  

## Goals of this project:
- To develop a template for any FOI portal to quickly, easily and economically create their own explanation animation - tailored for their site, their culture and audience. Also to allow people to further update and improve their animation as time goes on.


- To experiment and develop best practices for collaborative 2D animation using free software, and provide a _practical_ example for open sourcing animation, as well as using free/libre open source tools to create animation.
Right now, the documentation and arrangement of the project is very much set up for a user working with Inkscape and Synfig Studio with a graphical user interface - ideally in the future it could be possible to use a command line to replace specific text or other elements, automating changes which currently have to be made manually by clicking through the GUI.


- To improve the original video. There are a wide range of problems with the original which I would like to fix (in terms of timing, design, sound design as well as the general text and concept). However, animation is extremely time consuming and at some stage I simply had to say "OK, it's done" - I (literally) couldn't afford to keep tinkering. Through allowing others to take the project and improve it, or being commissioned to do so myself, I hope that changes and improvements can benefit all versions of the video.

## Project Overview
The project is split into 11 Synfig projects - every time there is a hard scene cut, rather than an animated transition, I use a new project. This means that some scenes are very short (eg '06 document handover') whereas others are very long and complex ('02 inside government' and '10 laptop')  

PROJECT LIST:  

01 title  
02 inside government  
03 post  
04 document types   
05 protestors sad   
06 document handover  
07 protestors happy  
08 multiple offices  
09 combining data  
10 laptop  
11 end title

Once I have the animation files in place, I will add the [Kdenlive](http://kdenlive.org) video editing project and the required audio files.


## Repository Structure

[/fonts](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/fonts)
A large part of adapting the project for international use is replacing German-language text with the local language, and changing the design/branding to suit a different FOI website. To do so, you can edit and replace the text in each scene's Inkscape .svg project.  
When converting an Inkscape .svg to a Synfig .sif, text is converted to vector paths, so you don't require these fonts if you're just working on the Synfig files, as they contain no text information.
 
[/notes](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/fonts)
Here I've noted the elements in each scene which (probably) need to be changed to adapt the original German video to a different language or country, and created some basic documentation on the structure and concept of each particular scene.

[/script](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/script)
Voiceover script and concept for the entire video. It will need to be translated for international use and I would like to improve it along the way!

[/sif](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/sif)
[Synfig file format](http://wiki.synfig.org/wiki/File_Formats)
The main animation files are [Synfig](http://www.synfig.org/) projects (.sif). There's one project for each scene in the video. 
Other than some linked texture files, the elements in the projects are all vector graphics, meaning they are simply mathematical expressions of points, lines, curves, shapes or polygons. 
This means that the projects are very flexible, lightweight*, and as the projects are text-based XML files, they lead themselves well to version control & scripting. 
To learn the basics of Synfig, I would recommend the official [Synfig Studio Training Course](https://gumroad.com/l/TkoN).  


[/svg](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/svg)
[Inkscape file format](http://wiki.inkscape.org/wiki/index.php/Frequently_asked_questions#Are_Inkscape.27s_SVG_documents_valid_SVG.3F)  
The illustrations for this video were created in [Inkscape](http://inkscape.org)  - mostly by me, but with many contributions from [Judith Carnaby](http://judithcarnaby.com).
Personally, I find Inkscape more flexible and intuitive for creating vector images than Synfig, so I create my images in Inkscape and then save a copy as a .sif:  
(File>Save As... Synfig Animation *.sif)  

[/textures](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/textures)
The original version of the video includes a number of public domain texture images (.jpg), which are overlaid on objects in Synfig. A texture can be imported (File>Import...) and placed directly above the groups or layers that you want to apply texture to. The 'Blending Mode' of the texture layer should be set to Overlay, and the 'Amount' (opacity) to something low, like 0,10.
The texture files are only _linked_ to the Synfig project, not embedded within the project. 

[/webm](https://gitlab.com/cameralibre/FragDenStaat-Animation/tree/master/webm)
Reference videos for each scene. 

## License

Frag den Staat is currently licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/). 

I'm not sure if this actually makes sense for project files, or just the cultural content - if a software license is more appropriate, please let me know!

## FOI portals around the world:
(let me know if I've missed any)

Australia  
Right To Know  
https://www.righttoknow.org.au/

Austria  
Frag Den Staat  
https://fragdenstaat.at

Bosnia  
Pravo Da Znam  
http://www.pravodaznam.ba/

Brazil  
Queremos Saber   
http://queremossaber.org.br/

Canada  
Je Veux Savoir  
http://www.jeveuxsavoir.org/

Croatia  
Imamo pravo znati   
http://imamopravoznati.org/

Czech Republic  
Info Pro Všechny  
http://www.infoprovsechny.cz/

Europe  
Ask the EU  
http://www.asktheeu.org/

Germany  
Frag Den Staat  
https://fragdenstaat.de

Hong Kong  
accessinfo.hk  
https://accessinfo.hk/

Hungary  
Ki Mit Tud  
http://kimittud.atlatszo.hu/

Israel  
Ask Data  
http://askdata.org.il/

Italy  
Diritto Di Sapere  
https://chiedi.dirittodisapere.it/

Macedonia  
Слободен пристап  
http://www.slobodenpristap.mk/

New Zealand  
FYI  
https://fyi.org.nz/

Romania  
Nu Vă Supărați  
http://nuvasuparati.info/

Rwanda  
Sobanukirwa  
https://sobanukirwa.rw/

Serbia  
Da Zanamo Svi  
http://daznamosvi.rs/sr

Spain  
Tu Derecho A Saber  
http://tuderechoasaber.es/ 

Sweden
Fråga Staten (coming soon!)
http://se.okfn.org/

Tunisia  
Marsoum 41  
http://www.marsoum41.org/en

Ukraine  
Доступ до правди  
https://dostup.pravda.com.ua/

United Kingdom  
WhatDoTheyKnow  
https://www.whatdotheyknow.com/

Uruguay  
Que Sabes  
http://www.quesabes.org/